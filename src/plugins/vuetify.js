import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        primary: '#4f2523',
        secondary: '#a1795a',
        accent: '#d6e0e6',
        error: '#FF5252',
        info: '#5f5c58',
        success: '#08051b',
        warning: '#FFC107',
      },
    },
  },
});
