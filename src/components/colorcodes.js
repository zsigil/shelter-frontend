export const colorcodes = {
  green: 'Jól szocializált, problémamentes',
  blue: 'Betegsége miatt különös figyelmet kell szánni rá!',
  yellow: 'Kisebb viselkedési problémák előfordulhatnak!',
  red: 'Komoly viselkedési zavarokkal rendelkezik!',
  pink: 'Pénzügyi támogatásra van szükségünk a kezelésére!',
  black: 'Elhunyt védenc',
};
