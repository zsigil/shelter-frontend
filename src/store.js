import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

import router from './router';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: true,

    animals: null,
    news: null,

    idToken: null,
    username: null,
    userCanAddNewAnimal: false,
    userCanChangeAnimal: false,
    userCanDeleteAnimal: false,
    userCanAddNewNewsitem: false,
    userCanChangeNewsitem: false,
    userCanDeleteNewsitem: false,
  },
  getters: {
    loading: state=> state.loading,

    allAnimals: state => state.animals,
    animalById: state => id => state.animals.filter(d=>d.id==id)[0],
    adoptableAnimals: state => state.animals.filter(d=>d.adoptable),

    allNews: state=> state.news,

    isAuthenticated: state=> state.idToken !== null,
    username: state=>state.username,
    token: state => state.idToken,
    hasPermissionToAddNew: state => state.userCanAddNewAnimal,
    hasPermissionToDelete: state => state.userCanDeleteAnimal,
    hasPermissionToChange: state => state.userCanChangeAnimal,
    hasPermissionToAddNewNewsitem: state => state.userCanAddNewNewsitem,
    hasPermissionToDeleteNewsitem: state => state.userCanDeleteNewsitem,
    hasPermissionToChangeNewsitem: state => state.userCanChangeNewsitem,
  },
  mutations: {
    setAnimals(state, payload){
      state.animals = payload;
      state.loading = false;
    },
    setNews(state, payload){
      state.news = payload;
    },
    loginUser(state, userData){
      state.idToken = userData.token;
      state.username = userData.username;
    },
    logoutUser(state){
      state.idToken = null;
      state.username = null;
      state.userCanAddNewAnimal = false;
      state.userCanChangeAnimal = false;
      state.userCanDeleteAnimal = false;
      state.userCanAddNewNewsitem = false;
      state.userCanChangeNewsitem = false;
      state.userCanDeleteNewsitem = false;
    },
    setUserGroupPermissions(state, allpermissionslist){
      state.userCanAddNewAnimal=false;
      state.userCanDeleteAnimal=false;
      state.userCanChangeAnimal=false;
      state.userCanAddNewNewsitem = false;
      state.userCanChangeNewsitem = false;
      state.userCanDeleteNewsitem = false;

      if (allpermissionslist.includes('shelter.add_animal')) {
        state.userCanAddNewAnimal=true;
      }
      if (allpermissionslist.includes('shelter.delete_animal')) {
        state.userCanDeleteAnimal=true;
      }
      if (allpermissionslist.includes('shelter.change_animal')) {
        state.userCanChangeAnimal=true;
      }
      if (allpermissionslist.includes('news.add_news')) {
        state.userCanAddNewNewsitem=true;
      }
      if (allpermissionslist.includes('news.delete_news')) {
        state.userCanDeleteNewsitem=true;
      }
      if (allpermissionslist.includes('news.change_news')) {
        state.userCanChangeNewsitem=true;
      }
    }
  },
  actions: {
    setAnimals:({commit,state})=>{
      state.loading = true;
      axios.get("/api/animals/")
      .then(res=>{
        commit('setAnimals', res.data);
      })
      .catch()
    },
    setNews:({commit,state})=>{
      axios.get("/api/news/")
      .then(res=>{
        commit('setNews', res.data);
      })
      .catch()
    },
    login({commit, dispatch}, payload){
      axios.post("/api-token-auth/", {username: payload.username, password: payload.password})
      .then(res=>{
        commit('loginUser', {token: res.data.token, username: payload.username});
        const router = payload.router;
        router.push('/administration');
        dispatch('setLogoutTimer');
        dispatch('setPermissions', res.data.token);
        const now = new Date()
        const expiresInSeconds = 60*60*2; //2 hours
        const expirationDate = now.getTime() + expiresInSeconds*1000;
        localStorage.setItem('shelterToken', res.data.token);
        localStorage.setItem('shelterUsername', payload.username);
        localStorage.setItem('shelterTokenExpires',new Date(expirationDate));
      })
      .catch()
    },
    tryAutoLogin({commit, dispatch}){
      const token = localStorage.getItem('shelterToken');
      const username = localStorage.getItem('shelterUsername');

      if(!token){
        dispatch('logout');
      }else{
        const expirationDate = new Date(localStorage.getItem('shelterTokenExpires'));
        const now = new Date()
        if(now >= expirationDate){
          dispatch('logout');
        }else{
          commit('loginUser', {token, username})
          dispatch('setPermissions', token);
        }
      }

    },
    logout({commit}){
      commit('logoutUser')
      localStorage.removeItem('shelterToken');
      localStorage.removeItem('shelterUsername');
      localStorage.removeItem('shelterTokenExpires');
      if (router.currentRoute.matched[0].path==='/administration') {
        router.push('/')
      }
    },
    setLogoutTimer({commit}){
      const expirationTimeSeconds = 60*60*2; //2 hours
      setTimeout(()=>{
        commit('logoutUser');
      }, expirationTimeSeconds*1000)
    },
    setPermissions({commit}, token){
      axios.get('/api/usergroups/', {headers:{'Authorization': `Token ${token}`}})
      .then(res=>{
        commit('setUserGroupPermissions', res.data[0]['allpermissions'])
      })
      .catch()
    }
  }
})
