import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import HelpUs from './views/HelpUs.vue'
import Adoption from './views/Adoption.vue'
import Administration from './views/Administration.vue'
import AnimalDetail from './components/AnimalDetail.vue'
import DemoInfo from './views/DemoInfo.vue'
import FAQ from './views/FAQ.vue'
import About from './views/About.vue'
import Contact from './views/Contact.vue'
import News from './views/News.vue'


import AddNewAnimal from './components/Administration/AddNewAnimal.vue'
import UpdateAnimal from './components/Administration/UpdateAnimal.vue'
import AddNewNews from './components/Administration/AddNewNews.vue'
import UpdateNews from './components/Administration/UpdateNews.vue'

import Login from './components/auth/Login.vue'

import store from './store'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: function (to) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }else{
      return { x: 0, y: 0 }
    }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/animal/:id',
      name: 'animaldetail',
      component: AnimalDetail,
    },
    {
      path: '/rolunk',
      name: 'rolunk',
      component: About,
    },
    {
      path: '/hirek',
      name: 'hirek',
      component: News,
    },
    {
      path: '/helpus',
      name: 'helpus',
      component: HelpUs
    },
    {
      path: '/orokbefogadas',
      name: 'orokbefogadas',
      component: Adoption
    },
    {
      path: '/kapcsolat',
      name: 'kapcsolat',
      component: Contact
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/administration',
      name: 'administration',
      component: Administration,
      beforeEnter(to, from, next){
        if(store.state.idToken !== null){
          next()
        }else{
          next('/login')
        }
      },
      children: [
        {
          path: 'new',
          component: AddNewAnimal,
        },
        {
          path: 'update/:id',
          component: UpdateAnimal,
        },
        {
          path: 'createnews',
          component: AddNewNews,
        },
        {
          path: 'updatenews/:id',
          component: UpdateNews,
        }
      ]
    },
    {
      path: '/demo',
      name: 'demo',
      component: DemoInfo,
    },
    {
      path: '/faq',
      name: 'faq',
      component: FAQ,
    },
    {
      path: '*',
      redirect: '/',
    },

  ]
})
